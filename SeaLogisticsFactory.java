package com.company;

public class SeaLogisticsFactory implements LogisticsFactory{
    @Override
    public Logistics createMachine(){
        return new SeaLogistics();
    }
}
